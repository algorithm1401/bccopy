function doesFileExist(urlToFile) {
    let xhr = new XMLHttpRequest();
    try {

        xhr.open('HEAD', urlToFile, false);
        xhr.send();

        return xhr.status != "404";
    } catch (e) {
        return false;
    }
}

const generateRealisations = () => {

    let lastSliderNumber = 1;
    let currentSliderPath = "assets/images/realistaties/" + lastSliderNumber + "_1";
    let realistationSliders = document.getElementById("realisationSliders");

    while (doesFileExist(currentSliderPath + ".jpg") || doesFileExist(currentSliderPath + ".png")) {

        let realistationSlider = document.createElement("div");
        realistationSlider.classList.add("ism-slider");
        realistationSlider.id = "ism-slider-" + (lastSliderNumber - 1);

        let frame = document.createElement("div");
        frame.classList.add("ism-frame");

        let ol = document.createElement("ol");
        ol.classList.add("ism-slides");
        ol.style.width = "200%";
        ol.style.perspective = "1000px";
        ol.style.backfaceVisibility = "hidden";
        ol.style.transform = "translateX(0px)";


        let currentImageNumber = 1;
        let currentImagePath = "assets/images/realistaties/" + lastSliderNumber + "_" + currentImageNumber;

        let isPng = false;
        isPng = doesFileExist(currentImagePath + ".png");

        while (isPng || doesFileExist(currentImagePath + ".jpg")) {

            let li = document.createElement("li");
            let img = document.createElement("img");
            if (isPng) {
                img.setAttribute("src", currentImagePath + ".png");
            } else {
                img.setAttribute("src", currentImagePath + ".jpg");
            }

            li.appendChild(img);
            ol.appendChild(li);

            currentImageNumber++;
            currentImagePath = "assets/images/realistaties/" + lastSliderNumber + "_" + currentImageNumber;
            isPng = doesFileExist(currentImagePath + ".png");
        }

        frame.appendChild(ol);
        realistationSlider.appendChild(frame);
        realistationSliders.appendChild(realistationSlider);

        lastSliderNumber++;
        currentSliderPath = "assets/images/realistaties/" + lastSliderNumber + "_1";
    }

};
document.addEventListener("DOMContentLoaded", generateRealisations);